<!DOCTYPE html>
<html>
	<head>
		<title>form.html</title>
		<meta charset="UTF-8">
	</head>
 
	<body>
		<h1>Buat Account Baru!</h1>
		<form action="/Welcome" method="POST">
		@csrf
		<h3>Sign Up Form</h3>
			<label for="Nama1">First Name:</label>
			<br><br>
			<input type="text" name="Nama1">
			<br><br>
			
			<label for="Nama2">Last Name:</label>
			<br><br>
			<input type="text" name="Nama2">
			<br><br>
			
			<label>Gender:</label>
			<br><br>
			<input type="radio" name="Gender" value="0">Male<br>
			<input type="radio" name="Gender" value="1">Female<br>
			<input type="radio" name="Gender" value="2">Other
			<br><br>
			
			<label>Nationality:</label>
			<br><br>
			<select>
			<option value="Indonesian">Indonesian</option>
			<option value="Singaporean">Singaporean</option>
			<option value="Malaysian">Malaysian</option>
			<option value="Australian">Australian</option>
			</select>
			<br><br>
			
			<label>Language Spoken:</label>
			<br><br>
			<input type="checkbox" placeholder="Language" value="0">Bahasa Indonesia<br>
			<input type="checkbox" placeholder="Language" value="1">English<br>
			<input type="checkbox" placeholder="Language" value="2">Other
			<br><br>
			
			<label>Bio:</label>
			<br><br>
			<textarea cols="30" rows="10"></textarea>
			<br>
			
			<a href="/Welcome"><button>Sign Up</button></a>
		</form>
	</body>
	
</html>